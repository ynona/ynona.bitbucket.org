#!/usr/bin/env python
# WTFPL 2.0
# Depends on Python (2 or 3) https://www.python.org/, Pillow https://pypi.python.org/pypi/Pillow/2.7.0
# Results viewable with this bookmarklet: http://ynona.bitbucket.org/reverse.url
# or this custom CSS (Firefox only): http://ynona.bitbucket.org/reverse.css

from PIL import Image
import sys

# reverse the bits in a byte (lookup table)
rev = [sum(((x>>i)&1)<<(7-i) for i in range(8)) for x in range(256)]

# usage
if len(sys.argv) != 3:
  print('usage: ' + sys.argv[0] + ' inputimage outputimage')
  sys.exit()

im = Image.open(sys.argv[1]).convert('RGB')
data = im.getdata()
data2 = [tuple(rev[x] for x in rgb) for rgb in data]
im2 = Image.new('RGB', im.size)
im2.putdata(data2)
im2.save(sys.argv[2])
