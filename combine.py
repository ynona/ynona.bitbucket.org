#!/usr/bin/env python
# WTFPL 2.0
# Depends on Python (2 or 3) https://www.python.org/, Pillow https://pypi.python.org/pypi/Pillow/2.7.0
# Results viewable with this bookmarklet: http://ynona.bitbucket.org/reverse.url
# or this custom CSS (Firefox only): http://ynona.bitbucket.org/reverse.css

from PIL import Image
import sys

# reverse the bits in a byte (lookup table)
rev = [sum(((x>>i)&1)<<(7-i) for i in range(8)) for x in range(256)]

# usage
if len(sys.argv) != 4:
  print('usage: ' + sys.argv[0] + ' hiddenimage topimage outputimage')
  sys.exit()

# read files
images = [Image.open(sys.argv[1+i]).convert('RGB') for i in range(2)]
size = images[0].size
if images[1].size != size:
  print('sizes different: {0} {1}'.format(images[0].size, images[1].size))
  sys.exit()
# extend to 16-bit, clamp to the guaranteed reachable range 0x0F00 - 0xF000
data = [[[x*0xE100//0xFF+0x0F00 for x in rgb] for rgb in im.getdata()] for im in images]
dataOut = []

x = y = 0
# loop over pixels
for rgb in zip(*data):
  rgbOut = []
  # and channels
  for c, val in enumerate(zip(*rgb)):
    # MAIN PART HERE: visible part on top, hidden part on bottom (reversed)
    valOut = rev[val[0] >> 12 << 4] | (val[1] >> 12 << 4)

    # dithering (Floyd-Steinberg); otherwise it looks like ass
    error = [val[0] - (rev[valOut] << 8), val[1] - (valOut << 8)]
    for dx, dy, w in [(1,0,7), (-1,1,3), (0,1,5), (1,1,1)]:
      if 0 <= x + dx < size[0] and 0 <= y + dy < size[1]:
        for i in (0, 1):
          data[i][(y+dy)*size[0]+x+dx][c] += (w * error[i]) >> 4

    rgbOut.append(valOut)
  dataOut.append(tuple(rgbOut))

  # update position
  x += 1
  if x == size[0]:
    x = 0
    y += 1

# write file
imageOut = Image.new('RGB', size)
imageOut.putdata(dataOut)
imageOut.save(sys.argv[3])
