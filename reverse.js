// reverse the bits in a byte (lookup table)
var rev = (function() {
  var n = [];
  for (var x = 0; x < 256; x++) {
    n[x] = 0;
    for (var i = 0; i < 8; i++) {
      n[x] |= ((x >> i) & 1) << (7 - i);
    }
  }
  return n;
})();

function reverseImage(data, dataOut) {
  for (var i = 0; i < data.data.length; i++) {
    var alpha = (i % 4 == 3);
    dataOut.data[i] = alpha ? 255 : rev[data.data[i]];
  }
}

function combineImages(data1, data2, dataOut) {
  var width  = data1.width;
  var height = data1.height;
  var nbytes = data1.data.length;

  // extend to 16-bit, clamp to the guaranteed reachable range 0x0F00 - 0xF000
  var data = [new Uint16Array(nbytes), new Uint16Array(nbytes)];
  for (var i = 0; i < nbytes; i++) {
    data[0][i] = data1.data[i] * 0xE100 / 0xFF + 0x0F00;
    data[1][i] = data2.data[i] * 0xE100 / 0xFF + 0x0F00;
  }

  // position, channel
  var x = 0;
  var y = 0;
  var c = 0;

  // directions and weights for dithering
  var dx = [1,-1, 0, 1];
  var dy = [0, 1, 1, 1];
  var w  = [7, 3, 5, 1];

  // loop over pixels and channels
  for (i = 0; i < nbytes; i++) {
    if (c == 3) {
      // skip alpha channel
      dataOut.data[i] = 255;
    } else {
      // MAIN PART HERE: hidden part on bottom (reversed), visible part on top
      var result = rev[data[0][i] >> 12 << 4] | (data[1][i] >> 12 << 4);
      dataOut.data[i] = result;

      // dithering (Floyd-Steinberg); otherwise it looks like ass
      var error0 = data[0][i] - (rev[result] << 8);
      var error1 = data[1][i] - (result << 8);
      for (var j = 0; j < 4; j++) {
        var x2 = x + dx[j];
        var y2 = y + dy[j];
        if (0 <= x2 && x2 < width && y2 < height) {
          var i2 = (y2*width + x2)*4 + c;
          data[0][i2] += (w[j] * error0) >> 4;
          data[1][i2] += (w[j] * error1) >> 4;
        }
      }
    }
    // update position, channel
    c++;
    if (c == 4) {
      c = 0;
      x++;
      if (x == width) {
        x = 0;
        y++;
      }
    }
  }
}

function fileToImage(file, cb) {
  var fr = new FileReader();
  fr.onload = function() {
    var blob = new Blob([fr.result], {type: file.type});
    var url = URL.createObjectURL(blob);
    var img = new Image();
    img.onload = function() {
      cb(img);
    };
    img.src = url;
  };
  fr.readAsArrayBuffer(file);
}

function makeContext(width, height) {
  var canvas = document.createElement('canvas');
  canvas.width = width;
  canvas.height = height;
  var ctx = canvas.getContext('2d');
  return ctx;
}

function fileToPixels(file, cb) {
  fileToImage(file, function(img) {
    var ctx = makeContext(img.width, img.height);
    ctx.drawImage(img, 0, 0);
    var data = ctx.getImageData(0, 0, img.width, img.height);
    URL.revokeObjectURL(img.src);
    cb(data);
  });
}

function makeImageData(width, height) {
  var ctx = makeContext(width, height);
  var data = ctx.getImageData(0, 0, width, height);
  return data;
}

function pixelsToURL(data, cb) {
  var canvas = document.createElement('canvas');
  canvas.width = data.width;
  canvas.height = data.height;
  var ctx = canvas.getContext('2d');
  ctx.putImageData(data, 0, 0);
  if (canvas.toBlob) {
    canvas.toBlob(function(blob) {
      var url = URL.createObjectURL(blob);
      cb(url);
    });
  } else {
    var url = canvas.toDataURL();
    var s = atob(url.split(',')[1]);
    var arr = new Uint8Array(s.length);
    for (var i = 0; i < s.length; i++) {
      arr[i] = s.charCodeAt(i);
    }
    var blob = new Blob([arr], {type: 'image/png'});
    var url = URL.createObjectURL(blob);
    cb(url);
  }
}

function showOutput(url) {
  var img = new Image();
  img.src = url;
  img.alt = 'Download';
  var link = document.createElement('a');
  link.appendChild(img);
  link.href = url;
  link.download = 'image.png';
  var output = document.getElementById('output');
  output.insertBefore(link, output.firstChild);
}

function clearInput() {
  var oldInput = this.nextElementSibling.firstElementChild
  var newInput = document.createElement('input');
  newInput.type = 'file';
  newInput.id = oldInput.id;
  oldInput.parentNode.replaceChild(newInput, oldInput);
}

function generate() {
  var input1 = document.getElementById('botfile');
  var input2 = document.getElementById('topfile');
  var file1 = input1.files[0];
  var file2 = input2.files[0];
  if (!file1) return;
  if (file2) {
    fileToImage(file1, function(img1) {
      fileToImage(file2, function(img2) {
        var width  = Math.max(img1.width,  img2.width  * img1.height / img2.height);
        var height = Math.max(img1.height, img2.height * img1.width  / img2.width);
        var ctx1 = makeContext(width, height);
        var ctx2 = makeContext(width, height);
        ctx1.drawImage(img1, (width - img1.width)/2, (height - img1.height)/2);
        ctx2.drawImage(img2, 0, 0, width, height);
        var data1 = ctx1.getImageData(0, 0, width, height);
        var data2 = ctx2.getImageData(0, 0, width, height);
        URL.revokeObjectURL(img1.src);
        URL.revokeObjectURL(img2.src);
        var dataOut = makeImageData(width, height);
        combineImages(data1, data2, dataOut);
        pixelsToURL(dataOut, showOutput);
      });
    });
  } else {
    fileToPixels(file1, function(data) {
      var dataOut = makeImageData(data.width, data.height);
      reverseImage(data, dataOut);
      pixelsToURL(dataOut, showOutput);
    });
  }
}

(function() {
  var clearbuttons = document.getElementsByClassName('clearbutton');
  clearbuttons[0].addEventListener('click', clearInput, false);
  clearbuttons[1].addEventListener('click', clearInput, false);
  var genbutton = document.getElementById('genbutton');
  genbutton.addEventListener('click', generate, false);
})();
